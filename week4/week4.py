from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit
from qiskit import IBMQ, Aer, execute
from qiskit.tools.visualization import plot_histogram, circuit_drawer

from qiskit.transpiler import PassManager
from qiskit.transpiler.passes import Unroller

#number of nodes
nNode = 4
#number of classical bits
nCBit = nNode * 2 + 1
#numbe of quantum bits
nQBit = nNode * 2 + 8
#first index used for OK calculation (2 qubits are used)
iUsedOK = nNode * 2
#first index store AND informaition
iABit = iUsedOK + 2

q = QuantumRegister(nQBit)
c = ClassicalRegister(nCBit)
qc = QuantumCircuit(q,c)

#util
def NXOR(a, b, c):
    qc.cx(b, c)
    qc.cx(a, c)
    qc.x(c)

def XOR(a, b, c):
    qc.cx(b, c)
    qc.cx(a, c)

#try not to use OR to avoid additional 2-qubit gates!
def OR(a, b, c):
    qc.cx(b, c)
    qc.cx(a, c)
    qc.ccx(a, b, c)

def AND(a, b, c):
    qc.ccx(a, b, c)

def NAND(a, b, c):
    qc.ccx(a, b, c)
    qc.x(c)

def InvNXOR(a, b, c):
    qc.x(c)
    qc.cx(a, c)
    qc.cx(b, c)

#check if 2 pairs of qubits are exactly the same or not(=company is the same)
#return 1: edge is valid = adjacent comapnies are different
#return 0: otherwise
#initializatin of qubits used for this calculation is included.
def OK(a, b, d):
    NXOR(a[0], b[0], q[iUsedOK])
    NXOR(a[1], b[1], q[iUsedOK + 1])
    NAND(q[iUsedOK], q[iUsedOK + 1], d)
    InvNXOR(a[1], b[1], q[iUsedOK + 1])
    InvNXOR(a[0], b[0], q[iUsedOK])
    qc.barrier()


#step1
qc.h(q[0:8])

#step2
OK(q[0:2], q[2:4], q[iABit])
OK(q[4:6], q[6:8], q[iABit + 1])

#step3
AND(q[iABit], q[iABit + 1], q[iABit + 2])

#measure
qc.measure(q[0:nNode * 2], c[0:nNode * 2])
qc.measure(q[iABit + 2], c[nNode * 2])