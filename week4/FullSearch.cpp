#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

vector< vector<int> > graph;
const int NV = 11;

bool check(int* label) {
   for (int i = 0; i < NV; i++) {
      for (auto g : graph[i]) {
         if (label[i] == label[g]) return false;
      }
   }
   return true;
}

int main() {
   graph.resize(NV);

   ifstream data;
   data.open("./edges.txt");
   int s, e;
   while (data >> s >> e) {
      graph[s].push_back(e);
      // graph[e].push_back(s);
   }
  
   int all = 0;
   int ans = 0;
   int label[NV];
   for (int i0 = 0; i0 < 4; i0++) {
      for (int i1 = 0; i1 < 4; i1++) {
         for (int i2 = 0; i2 < 4; i2++) {
            for (int i3 = 0; i3 < 4; i3++) {
               for (int i4 = 0; i4 < 4; i4++) {
                  for (int i5 = 0; i5 < 4; i5++) {
                     for (int i6 = 0; i6 < 4; i6++) {
                        all++;
                        memset(label, -1, sizeof(label));
                        label[0] = i0;
                        label[1] = i1;
                        label[2] = i2;
                        label[3] = i3;
                        label[4] = i4;
                        label[5] = i5;
                        label[6] = i6;
                        label[7] = 0;
                        label[8] = 1;
                        label[9] = 2;
                        label[10] = 3;
                        if (!check(label)) continue;
                        for (int i = 0; i < NV; i++) cout << label[i] << " ";
                        cout << endl;
                        ans++;
                     }
                  }
               }
            }
         }
      }
   }
   cout << ans << " out of " << all << " is selected" << endl;
   return 0;
}
